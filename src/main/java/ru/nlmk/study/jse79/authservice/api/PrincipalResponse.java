package ru.nlmk.study.jse79.authservice.api;

import lombok.Data;

import java.util.List;

@Data
public class PrincipalResponse {

    public PrincipalResponse() {
    }

    public PrincipalResponse(String principal, List<String> roles) {
        this.principal = principal;
        this.roles = roles;
    }

    private String principal;

    private List<String> roles;
}
