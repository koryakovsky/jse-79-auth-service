package ru.nlmk.study.jse79.authservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.nlmk.study.jse79.authservice.api.PrincipalResponse;
import ru.nlmk.study.jse79.authservice.api.TokenRequest;
import ru.nlmk.study.jse79.authservice.service.AuthService;

@RestController
public class AuthController {

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/auth")
    public ResponseEntity<PrincipalResponse> auth(@RequestBody TokenRequest tokenRequest) {

        String token = tokenRequest.getToken();
        if (token == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        PrincipalResponse response = authService.validateAndGetPrincipal(token);

        if (response == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        } else {
            return ResponseEntity.ok(response);
        }
    }
}
