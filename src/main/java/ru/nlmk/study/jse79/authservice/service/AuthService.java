package ru.nlmk.study.jse79.authservice.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.nlmk.study.jse79.authservice.api.PrincipalResponse;

import java.nio.charset.StandardCharsets;
import java.util.List;

@Service
public class AuthService {

    @Value("${jwt.token.secret-key}")
    private String jwtSecret;

    public PrincipalResponse validateAndGetPrincipal(String token){
        try {
            Claims claims = Jwts.parserBuilder()
                    .setSigningKey(Keys.hmacShaKeyFor(jwtSecret.getBytes(StandardCharsets.UTF_8)))
                    .build()
                    .parseClaimsJws(token)
                    .getBody();
            String principal = claims.getSubject();

            List<String> roles = List.of("ROLE_USER");

            return new PrincipalResponse(principal, roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
